//
//  RMainViewController.swift
//  SideMenuDemo
//
//  Created by Eugene Migun on 01/02/2017.
//  Copyright © 2017 BP Mobile. All rights reserved.
//

import UIKit

class RMainViewController: UIViewController
{
    override func viewDidLoad()
	{
		print("viewDidLoad: \(self)")
		
        super.viewDidLoad()
    }

	override func viewWillAppear(_ animated: Bool)
	{
		print("viewWillAppear: \(self)")
		
		super.viewWillAppear(animated)

		//делаем градиент на весь экран, типа как у конкурента (пока не очень похоже)
		let gradient: CAGradientLayer = CAGradientLayer()
		gradient.colors = [UIColor.red.cgColor, UIColor.white.cgColor]
		gradient.locations = [0.0 , 1.0]
		gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
		gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
		gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
		self.view.layer.insertSublayer(gradient, at: 0)
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
