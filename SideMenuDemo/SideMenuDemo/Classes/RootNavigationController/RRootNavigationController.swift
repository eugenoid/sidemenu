//
//  RRootNavigationController.swift
//  SideMenuDemo
//
//  Created by Eugene Migun on 2.2.2017.
//  Copyright © 2017 BP Mobile. All rights reserved.
//

import UIKit

class RRootNavigationController: UINavigationController, UINavigationControllerDelegate
{

    override func viewDidLoad()
	{
        super.viewDidLoad()

		//будем ловить сообщение willShow viewController, чтобы прятать navigationBar
		self.delegate = self;
    }

	func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool)
	{
		print("willShow: \(viewController)")
		
		//для главного экрана прячем UINavigationBar, для остальных показываем
		if(viewController is RMainViewController)
		{
			print("hide navigation bar")
			self.setNavigationBarHidden(true, animated: animated)
		}
		else
		{
			print("shownavigationbar")
			self.setNavigationBarHidden(false, animated: animated)
			
			//убираем текст back button на всех контроллерах, там будет картинка
			let count = viewController.navigationController?.viewControllers.count;
			if(count! >= 2)
			{
				viewController.navigationController?.viewControllers[count! - 2].navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
			}
		}
	}
}
