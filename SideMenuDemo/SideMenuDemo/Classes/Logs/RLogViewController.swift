//
//  RLogViewController.swift
//  SideMenuDemo
//
//  Created by Eugene Migun on 01/02/2017.
//  Copyright © 2017 BP Mobile. All rights reserved.
//

import UIKit

class RLogViewController: UIViewController
{
	deinit
	{
		print("deinit: \(self)")
	}
	
    override func viewDidLoad()
	{
		print("viewDidLoad: \(self)")
        super.viewDidLoad()
    }
	
	override func viewWillAppear(_ animated: Bool)
	{
		print("viewWillAppear: \(self)")
		super.viewWillAppear(animated)
	}
	
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
	{
		print("prepare for: \(segue)")
    }

}
